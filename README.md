# docabbygriffin

@[docabbygriffin](http://twitter.com/docabbygriffin)

I watch the show "The 100" on the CW, which has a lot of heavy, obvious
moralizing. I decided to make a bot that delivered a new moral from the show
every few hours. Templates are all lines taken from the show with a random
word of the same part of speech replacing them.

I eventually want to replace some of the repeated code between the different
morals with a more general function that will make generating new templates
much easier.
