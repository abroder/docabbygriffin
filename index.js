#!/usr/bin/env node

var Twit = require('twit')
var Wordnik = require('wordnik')
var pluralize = require('pluralize')
var config = require('./config')

var T = new Twit(config.Twit)
var wn = new Wordnik(config.Wordnik)

var sendTweet = function (status) {
  T.post('statuses/update', {status: status, lat: '38.420029', long: '-78.929841'}, function (err, data, response) {
    if (err) {
      console.log(`Problem posting status: ${ err }`)
      return
    }
  })
}

var generateGoodPeople = function () {
  wn.randomWord({includePartOfSpeech: 'noun'}, function (err, res) {
    var status = `Maybe there are no good ${ pluralize.plural(res.word) }.`
    sendTweet(status)
  })
}

var generateInnocence = function () {
  wn.randomWord({includePartOfSpeech: 'adjective'}, function (err, res) {
    var status = `None of us is ${ res.word }.`
    sendTweet(status)
  })
}

var generateAlphabet = function () {
  wn.randomWords({includePartOfSpeech: 'noun', limit: 3}, function (err, res) {
    var firstWord = res[0].word.charAt(0).toUpperCase() + res[0].word.slice(1)
    var status = `${ firstWord }, ${ res[1].word }, ${ res[2].word }, those are the ABCs of me. Get rid of them and there's nothing left.`
    sendTweet(status)
  })
}

var generatePlaces = function () {
  wn.randomWords({includePartOfSpeech: 'noun', limit: 2}, function (err, res) {
    var firstWord = pluralize.plural(res[0].word.charAt(0).toUpperCase() + res[0].word.slice(1))
    var secondWord = pluralize.plural(res[1].word.charAt(0).toUpperCase() + res[1].word.slice(1))
    var status = `${ firstWord } aren't evil, brother. ${ secondWord } are.`
    sendTweet(status)
  })
}

var generateGoal = function () {
  wn.randomWords({includePartOfSpeech: 'noun', limit: 1}, function (err, res) {
    var word = res[0].word
    var status = `The ${ word } isn't everything. How you reach the ${ word } matters, too. I'm sorry I didn't teach you that.`
    sendTweet(status)
  })
}

var generateWhatIDo = function () {
  wn.randomWord({includePartOfSpeech: 'verb'}, function (err, res) {
    var status = `I ${ res.word } it, so they don't have to.`
    sendTweet(status)
  })
}

var rules = [generateGoodPeople, generateInnocence, generateAlphabet, generatePlaces, generateGoal]

var generateMoral = function() {
  rules[Math.floor((Math.random() * rules.length))]()
}

generateMoral()
